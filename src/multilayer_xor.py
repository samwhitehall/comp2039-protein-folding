from pybrain.tools.shortcuts import buildNetwork
from pybrain.datasets import SupervisedDataSet 
from pybrain.supervised.trainers import BackpropTrainer

net = buildNetwork(2, 4, 1, bias=True)
ds = SupervisedDataSet(2, 1)

ds.appendLinked((0,0), (0,))
ds.appendLinked((1,0), (1,)) 
ds.appendLinked((0,1), (1,))  
ds.appendLinked((1,1), (0,))

trainer = BackpropTrainer(net, learningrate=0.01, momentum=0.99, verbose=True)
trainer.trainOnDataset(ds, 1000)
# trainer.testOnData()
trainer.trainUntilConvergence()
