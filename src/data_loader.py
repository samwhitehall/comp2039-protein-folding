class DataLoader():
	'''
	DataLoader loads data in the format given by Qian and Sejnowski, and convert it to a unified,
	sensible format that can be handled in Python.
	'''
	
	class Protein():
		'''Made up of a collection of amino-acid -> substructure pairs'''
		def __init__(self):
			self.pairs = []
			
				
	def __init__(self, file_location="data/protein-secondary-structure.train.txt"):
		'''Initialisation code'''
		self.proteins = []
		self.windows = []
		self.file_location = file_location
		
		self.load_proteins()
		self.generate_unary_encoding()
	
	def load_proteins(self):
		'''Convert the Qian and Sejnowski protein file into a less crazy internal representation'''
		file = open(self.file_location, 'r')
		
		for i, line in enumerate(file):
			line = line.rstrip() # strip newline character 
			
			if line.startswith("#") or line == "": # this line is a comment
				continue
			elif line.startswith("<>"):	# protein start delimiter
				current_protein = self.Protein() # new protein
			elif line.startswith("<end>") or line.startswith("end"): # protein end delimiter
				self.proteins.append(current_protein) # add to list
			else: # add current pair to protein
				current_protein.pairs.append(tuple(line.split(" ")))
				
	def create_windows(self, window_size=5):
		'''Generate sliding windows of amino strings -> central substructure'''
		for protein in self.proteins:
			for i in range(len(protein.pairs) - window_size):
				amino_string = "".join([amino_acid for amino_acid, structure in protein.pairs[i:i + window_size]])
				central_substructure = protein.pairs[i + window_size/2][1]
				self.windows.append((amino_string, central_substructure))
			
	def generate_unary_encoding(self):
		'''Personal method to create unary encoding lookup table'''
		file = open(self.file_location, 'r')
		
		amino_acids = set()
		for line in file:
			line = line.rstrip()
			if not( line.startswith("#") or line == "" or line == "<end>" or line == "end" or line == "<>" ):
				amino_acids.add(line.split(" ")[0])
				
		self.encoding = {}
		for i, amino_acid in enumerate(amino_acids):
			self.encoding[amino_acid] = [0 if number != i else 1 for number in range(20)]
			
	def get_unary_encoding(self, amino_sequence):
		'''Get the unary encoding of an amino acid sequence'''
		result = []
		for amino_acid in amino_sequence:
			result += self.encoding[amino_acid]
		return result