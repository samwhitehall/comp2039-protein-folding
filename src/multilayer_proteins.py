from pybrain.tools.shortcuts import buildNetwork
from pybrain.datasets import SupervisedDataSet, ClassificationDataSet 
from pybrain.supervised.trainers import BackpropTrainer

import data_loader

protein_net = buildNetwork(100, 40, 3, bias=True)

raw_data = data_loader.DataLoader()
raw_data.create_windows(window_size=5)

all_data = ClassificationDataSet(100)

class_mapping = { 'h':0, 'e':1, '_':2}
for window in raw_data.windows:
	unary_encoding = raw_data.get_unary_encoding(window[0])
	all_data.addSample(unary_encoding, [class_mapping[window[1]]])

all_data._convertToOneOfMany()
test_data, train_data = all_data.splitWithProportion(0.25) # 25% test data

test_data._convertToOneOfMany()
train_data._convertToOneOfMany()

trainer = BackpropTrainer(protein_net, learningrate=0.1, momentum=0.1, verbose=True)
trainer.trainOnDataset(train_data, 20)
