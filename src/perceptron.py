import data_loader
import math, random

class Perceptron():
	'''
	Toy Perceptron partially inspired by http://en.wikipedia.org/wiki/Perceptron
	 - creates a neural net that functions as a linear classifier between linearly separable variables
	'''
	
	def __init__(self, threshold=0.5, learning_rate=0.1, input_size=20, default_weight=0):
		'''Initialisation code'''
		self.threshold = threshold
		self.learning_rate = learning_rate
		self.input_size = input_size
		self.training_set = []
		self.test_set = []
		self.weights = [0 for i in range(input_size + 1)]
		
	def get_output(self, input_vector):
		'''Returns the binary output for a given input vector (tuple) on the perceptron'''
		return 1 if sum(value * self.weights[index] for index, value in enumerate(input_vector)) > self.threshold else 0
				
	def populate_set(self, set, data, classifier="h", subset_size=400):		
		# match sizes of subsets
		positives = [(amino_seq, central_structure) for amino_seq, central_structure in data.windows if central_structure == classifier]
		negatives = [(amino_seq, central_structure) for amino_seq, central_structure in data.windows if central_structure != classifier]

		# select a random sample of negatives to match sizes
		positive_sample = random.sample(positives, subset_size)
		negative_sample = random.sample(negatives, subset_size)

		for i in range( len(positive_sample) ):
			(am1, cs1) = positive_sample[i]
			self.add_to_set(set, input_vals=data.get_unary_encoding(am1), expected_value=1)
			(am2, cs2) = negative_sample[i]
			self.add_to_set(set, input_vals=data.get_unary_encoding(am2), expected_value=0)
				
	def add_to_set(self, set, input_vals, expected_value, bias=1):
		'''Add an input vector (tuple) and its expected value to the training set'''
		if len(input_vals) != self.input_size:
			raise Exception('Incorrect Vector Size: is %d, should be %d' % (len(input_size), self.input_size) )

		# prepend bias value to input values, before adding to training set
		set.append(((bias,) + tuple(input_vals), expected_value) )		
		
	def train(self, iterations, threshold=0):
		'''Perceptron training algorithm (), terminating after error < threshold'''
		for i in range(iterations):
			# print "*" * 20
			# print self.weights
			error_count = 0
			
			for input_vector, expected_value in self.training_set:
				output = self.get_output(input_vector)
				# print ("In: %s\t Out: %d\t Exp: %d" % (str(input_vector), output, expected_value))
				error = expected_value - output
				if error != 0:
					error_count += 1
					for index, value in enumerate(input_vector):	# set new weights, approaching optimal value
						self.weights[index] += self.learning_rate * error * value 
			# print "err count: %d" % error_count
			
			if error_count == threshold: # terminate once weights completely classify input data
				break
				
	def q3(self, test_set):
		'''Calculate the percentage of correctly classified residues (Q3)'''
		assigned, correctly_assigned = 0.0, 0.0
		for input_vector, expected in test_set:
			if self.get_output(input_vector):
				assigned += 1.0
				if expected:
					correctly_assigned += 1.0
					
		if assigned == 0:
			return 0
		else:
			return (correctly_assigned / assigned) * 100
		
				
	def correlation_coefficient(self, test_set):
		'''Calculate value Cx, s.t. -1 < Cx < 1, that represents the correlation coefficient'''
		p, n, o, u = 0.0, 0.0, 0.0, 0.0
		
		for input_vector, expected in test_set:
			assigned = self.get_output(input_vector)
			if assigned and expected: #correctly assigned in category
				p += 1
			elif not assigned and not expected: #correctly assigned not in category
				n += 1
			elif assigned and not expected: #incorrectly assigned in category
				o += 1
			elif not assigned and expected: #incorrectly assigned not in category
				u += 1
			
		# print ("Correctly Assigned: %d" % p)
		# print ("Correctly Not Assigned: %d" % n)
		# print ("Incorrectly Assigned: %d" % o)
		# print ("Incorrectly Not Assigned: %d" % u)
		
		# TODO: what to do about 0 in denominator?
		if(p==0 or o==0 or u==0 or n==0):
			return -1
		else:
			return ((p * n) - (o * u)) / math.sqrt((p + o) * (p + u) * (n + o) * (n + u))
