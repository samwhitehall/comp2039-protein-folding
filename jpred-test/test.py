from __future__ import division

seq = "ENLKLGFLVKQPEEPWFQTEWKFADKAGKDLGFEVIKIAVPDGEKTLNAIDSLAASGAKGFVICTPDPKLGSAIVAKARGYDMKVIAVDDQFVNAKGKPMDTVPLVMMAATKIGERQGQELYKEMQKRGWDVKESAVMAITANELDTARRRTTGSMDALKAAGFPEKQIYQVPTKSNDIPGAFDAANSMLVQHPEVKHWLIVGMNDSTVLGGVRATEGQGFKAADIIGIGINGVDAVSELSKAQATGFYGSLLPSPDVHGYKSSEMLYNWVAKDVEPPKFTEVTDVVLITRDNFKEELEKKGLGGK"
prd = "--EEEEEEE-----HHHHHHHHHHHHHHHH---EEEEE----HHHHHHHHHHHHH----EEEEE-------HHHHHHHHH----EEEEE---------------EEE--HHHHHHHHHHHHHHHH-----------EEE--------HHHHHHHHHHHHHH------EEEEEE-----HHHHHHHHHHHHHH-----EEEEEE---HHHHHHHHHHHH-------EEEEEE---HHHHHHH------EEEEEE--HHHHHHHHHHHHHHHHH--------EEEE--EEEE---HHHHHHHH-----"
act = "______eee_______hhhhhhhhhhhhhh______eee___hhhhhhhhhhhhh_________________hhhhhhhhh_____________________________hhhhhhhhhhhhhhhhhh________eee_________hhhhhhhhhhhhhh_______ee________hhhhhhhhhhhhh_______eee_______hhhhhhhh____________ee_____hhhh__________ee_______hhhhhhhhhhhhh__________________________________"

if __name__ == '__main__':
	length = len(seq)
	total = 0
	
	for i in range(length):
		if (prd[i].lower() == act[i]) or (prd[i] is "-" and act[i] is "_"):
			print "%c %c" % (prd[i].lower(), act[i])
			total += 1
	
	percent_correct = (total / length) * 100
	
	print "%d%% correct" % percent_correct